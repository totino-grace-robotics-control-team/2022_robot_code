/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

//Clock subsysyem, which exists to format SmartDashboard displays
public class Clock extends SubsystemBase {
  public Clock() {}

  //Declarations
  public String gamePeriod = "Auto";

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    SmartDashboard.putNumber("", getMinutes());
    SmartDashboard.putNumber(":", getSeconds());
    SmartDashboard.putString("Time ", getPeriod());
  }

  public Integer getMinutes(){
    return (int) Timer.getMatchTime()/60;
  }
  
  public Integer getSeconds(){
    return (int) Math.floor(Timer.getMatchTime()%60);
  }

  public String getPeriod(){
    if(Timer.getMatchTime() >= 35){
      gamePeriod = "Tele";
    }
    return gamePeriod;
  }
}
