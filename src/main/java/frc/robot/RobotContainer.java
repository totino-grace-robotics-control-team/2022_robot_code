/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.Constants.joystickConstants;
import frc.robot.Constants.usbPortConstants;
// import frc.robot.commands.climber.*;
import frc.robot.commands.drivetrain.*;
import frc.robot.commands.elevator.LowerElevator;
import frc.robot.commands.elevator.PerformClimb;
import frc.robot.commands.elevator.RaiseElevator;
import frc.robot.commands.elevator.ReleaseString;
import frc.robot.commands.elevator.TightenString;
import frc.robot.commands.intakeShooter.*;
import frc.robot.commands.vision.*;
import frc.robot.subsystems.*;
// import frc.robot.commands.ballPastIntakeTest;
import frc.robot.commands.autonomous.AutonomousCommandGroup;
//import frc.robot.commands.PneumaticsTest;



/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */

public class RobotContainer {
	//Subsystems
	private final IntakeAndShoot m_intakeAndShoot = new IntakeAndShoot();
	// TODO private final Elevator m_elevator = new Elevator();
	private final Drivetrain m_driveTrain = new Drivetrain();
	private final Aim m_aim = new Aim();
	private final Elevator m_elevator = new Elevator();
	//private final PneumaticSub m_airSubsys = new PneumaticSub();

	private final CameraManager m_cameraManager = new CameraManager();

	//Controllers
	private final Joystick m_driveJoystick = new Joystick(usbPortConstants.kDriveJoystick);
	private final Joystick m_weaponsJoystick = new Joystick(usbPortConstants.kWeaponsJoystick);

	//The container for the robot. Contains subsystems, OI devices, and commands.
	public RobotContainer() {
		//Set default drive command to joystick control
		m_driveTrain.setDefaultCommand(
			new RunCommand(() -> {
				m_driveTrain.drive(m_driveJoystick); 
			}, m_driveTrain)
		);
		
		m_aim.setDefaultCommand(
			new RunCommand(() -> {
				m_aim.aimMotorManually(m_weaponsJoystick); 
			}, m_aim)
		);


		m_elevator.setDefaultCommand(
			new RunCommand(() -> {
				m_elevator.elevatorMotorManually(m_weaponsJoystick); 
			}, m_elevator)
		);
		

		//Configure button bindings, as defined below
		configureButtonBindings();

		//Add subsystems to SmartDashboard
	
		SmartDashboard.putData(m_intakeAndShoot);
		//SmartDashboard.putData(m_shoot);
		// SmartDashboard.putData(m_cwm);
		//SmartDashboard.putData(m_climber);
		SmartDashboard.putData(m_driveTrain);
		SmartDashboard.putData(m_cameraManager);
	}

	//Define button mapping
	private void configureButtonBindings() {
		//Drive Controller - Drivetrain
			//Toggle polarity of Drivetrain controls
			final JoystickButton m_drivePolarityToggle = new JoystickButton(m_driveJoystick, joystickConstants.kToggleDriveButton);
			m_drivePolarityToggle.whenPressed(new toggleDrive(m_driveTrain));



			// m_drivePolarityToggle.whileHeld(new PunchOnTest(m_aim));
			// m_drivePolarityToggle.whenReleased(new PunchOffTest(m_aim));

		// //Drive Controller - Climber (including Periscope and Winch)
		// 	//Deploy the Periscope, while the button is held
		// 	final JoystickButton m_unlimitedUp = new JoystickButton(m_driveJoystick, buttonConstants.kDeployPeriscopeButton);
		// 	m_unlimitedUp.whenHeld(new PeriscopeUp(m_climber));

		// 	//Un-deploy, or retract, the Periscope, while the button is held
		// 	final JoystickButton m_unlimitedDown = new JoystickButton(m_driveJoystick, buttonConstants.kUndeployPeriscopeButton);
		// 	m_unlimitedDown.whenHeld(new PeriscopeDown(m_climber));

		// 	//Add a command to deploy until the encoder indicates it's at the top

		// 	//Add a command to undeploy until the encoder indicates it's at the bottom	

		// 	//Fly, or pull the robot up by retracting the winch, while button is held
		// 	final JoystickButton mc_Fly = new JoystickButton(m_driveJoystick, buttonConstants.kFlyButton);
		// 	mc_Fly.whenHeld(new WinchIn(m_climber));

		//Drive Controller - Camera
			//Flip to the next camera view
			// final JoystickButton m_cameraSwitch = new JoystickButton(m_driveJoystick, joystickConstants.kToggleCameraButton);
			// m_cameraSwitch.whenPressed(new ToggleCamera(m_cameraManager)); //needs to be renamed to something reasonable

		//Weapons Controller - ICS (including Intake, Conveyor, and Shooter)
			//Shoot while button is held
			// final JoystickButton m_icsShoot = new JoystickButton(m_weaponsJoystick, buttonConstants.kIcsShootButton);
			// m_icsShoot.whenPressed(new ShootContinuous(m_ics));
			// m_icsShoot.whenReleased(new StopShoot(m_ics)); //this could be one whenHeld instead

			// //Intake one ball
			final JoystickButton IntakeBTN = new JoystickButton(m_weaponsJoystick, Constants.WeaponsConstants.kIntakeButton);
			Command saveme = new IntakeBall(m_intakeAndShoot);
			IntakeBTN.whileHeld(saveme);

			//final JoystickButton intakeCancel = new JoystickButton(m_weaponsJoystick, Constants.WeaponsConstants.kIntakeButton);
			IntakeBTN.whenReleased(new RunCommand(() -> {
				System.out.print("Save me before if");
				if (saveme != null) {
					System.out.println("Save me after if");
					saveme.cancel();
					//saveme = null; TODO figure out how to clean up
				}
			}, m_intakeAndShoot));


			//  Command cancelIntake = new RunCommand(() -> {
			//  	if (saveme != null) {
			//  		saveme.cancel();
			//  	}
			//  }, m_intakeAndShoot);

			// //Not sure precisely what this does, although it's clearly stopping the ICS to some degree
			// final JoystickButton m_icsCancel = new JoystickButton(m_weaponsJoystick, buttonConstants.kIcsCancelButton);
			// m_icsCancel.whenPressed(new stopICSSpin(m_ics)); //needs investigation

			
			final JoystickButton PerformShootBTN = new JoystickButton(m_weaponsJoystick, Constants.WeaponsConstants.kShootButton);
			PerformShootBTN.whenPressed(new PerformShoot(m_intakeAndShoot, m_aim));


			// put a whenReleased to interupt the shoot command 
		

			final JoystickButton increaseShootBTN = new JoystickButton(m_driveJoystick, Constants.joystickConstants.kIncreaseShootButton);
			increaseShootBTN.whenPressed(new IncreaseMotorSpeed(m_intakeAndShoot));

			final JoystickButton decreaseShootBTN = new JoystickButton(m_driveJoystick, Constants.joystickConstants.kDecreaseShootButton);
			decreaseShootBTN.whenPressed(new DecreaseMotorSpeed(m_intakeAndShoot));

			// move aim  to a fixed location
			final JoystickButton fixedAimN25BTN = new JoystickButton(m_driveJoystick, Constants.joystickConstants.kAimFixedN25Button);
			fixedAimN25BTN.whenPressed(new AimFixed(m_aim, -25));

			final JoystickButton fixedAimN8BTN = new JoystickButton(m_driveJoystick, Constants.joystickConstants.kAimFixedN8Button);
			fixedAimN8BTN.whenPressed(new AimFixed(m_aim, -8));

			final JoystickButton fixedAimN15BTN = new JoystickButton(m_driveJoystick, Constants.joystickConstants.kAimFixedN15Button);
			fixedAimN15BTN.whenPressed(new AimFixed(m_aim, -15));

			final JoystickButton fixedAimN19BTN = new JoystickButton(m_driveJoystick, Constants.joystickConstants.kAimFixedN19Button);
			fixedAimN19BTN.whenPressed(new AimFixed(m_aim, -19));

			final JoystickButton fixedAimN15TESTBTN = new JoystickButton(m_driveJoystick, 8);
			fixedAimN15TESTBTN.whenPressed(new AimFixedN15(m_aim));

			// final JoystickButton raiseElevatorBTN = new JoystickButton(m_weaponsJoystick, Constants.WeaponsConstants.kRaiseElevatorButton);
			// raiseElevatorBTN.whenPressed(new RaiseElevator(m_elevator));

			// final JoystickButton lowerElevatorBTN = new JoystickButton(m_weaponsJoystick, Constants.WeaponsConstants.kLowerElevatorButton);
			// lowerElevatorBTN.whenPressed(new LowerElevator(m_elevator));

			final JoystickButton tightenStringBTN = new JoystickButton(m_weaponsJoystick, Constants.WeaponsConstants.kTightenStringButton);
			tightenStringBTN.whenPressed(new TightenString(m_elevator).withTimeout(.25));
		
			final JoystickButton performClimbBTN = new JoystickButton(m_weaponsJoystick, Constants.WeaponsConstants.kPerformClimb);
			performClimbBTN.whenPressed(new PerformClimb(m_elevator));

			final JoystickButton releaseStringBTN = new JoystickButton(m_weaponsJoystick, Constants.WeaponsConstants.kReleaseStringButton);
			releaseStringBTN.whenPressed(new ReleaseString(m_elevator).withTimeout(.25));

			///_weaponsJoystick, buttonConstants.kIcsCancelButton);
			// puncherExtend.whenPressed(new PerformShoot(m_intakeAndShoot)); //for testing purposes, need another button set to retract

			// final JoystickButton puncherRetract = new JoystickButton(m_weaponsJoystick, 7);
			// puncherRetract.whenPressed(new AimFixed(m_aim, 360));



			// Below are test commands to check the specific commands
			
			// final JoystickButton conveyTest = new JoystickButton(m_driveJoystick, buttonConstants.kUndeployPeriscopeButton);
			// conveyTest.whenPressed(new ballPastIntakeTest(m_intakeAndShoot));
			
			//final JoystickButton releaseString = new JoystickButton(m_driveJoystick, Constants.WeaponsConstants.kpuncherRetractButton);
			//puncherRetract.whenPressed(new RetractPuncher(m_intakeAndShoot).withTimeout(.2));

			//releaseString.whenPressed(new ReleaseString(m_elevator).withTimeout(.15));

			// final JoystickButton pneumaticsTest = new JoystickButton(m_weaponsJoystick, buttonConstants.kUndeployPeriscopeButton);
			// pneumaticsTest.whenPressed(new PneumaticsTest(m_airSubsys));

			// puncherRetract.whenPressed(new StopShoot(m_intakeAndShoot));

			SmartDashboard.putData("Autonomous Command", new AutonomousCommandGroup(m_aim, m_intakeAndShoot, m_driveTrain));
			SmartDashboard.putData("Release String", new ReleaseString(m_elevator));
			SmartDashboard.putData("Tighten String", new TightenString(m_elevator));
			SmartDashboard.putData("Aim Fixed", new AimFixed(m_aim, -25));
			SmartDashboard.putData("Intake Ball", new IntakeBall(m_intakeAndShoot));
			SmartDashboard.putData("Begin Shoot", new BeginShoot(m_intakeAndShoot));
			SmartDashboard.putData("Extend Puncher", new ExtendPuncher(m_intakeAndShoot).withTimeout(0.125));
			SmartDashboard.putData("Retract Puncher", new RetractPuncher(m_intakeAndShoot).withTimeout(0.15));
			SmartDashboard.putData("Stop Shoot", new StopShoot(m_intakeAndShoot));
			SmartDashboard.putData("Reset Aim", new ResetAimEncoder(m_aim));

			// SmartDashboard.putData("EXTEND PUNCHER", new ExtendPuncher(m_intakeAndShoot).withTimeout(0.125));
			// SmartDashboard.putData("RETRACT PUNCHER", new RetractPuncher(m_intakeAndShoot).withTimeout(0.15));

			//put stuff in smart dashboard so that when you click on the button, the commands run
			//so now we don't have to make test buttons on the controllers!
			//super fun!


	}

	//Return the command to run in Autonomous Mode here
	public Command getAutonomousCommand() {
		//return new RunCommand(() -> {}); //run the command here, inside them brackets
		return new AutonomousCommandGroup(m_aim, m_intakeAndShoot, m_driveTrain);
		/**
		 Since we'll be dong a lot, this should call a command group or process
		 instead of the current anonymous function.

		 Things to do in autonomous:
		 1. Start the Shooter
		 2. Wait for the Shooter to reach top speed
		 (Potentially run a targeting routine here, depending on how long it takes to run)
		 3. Run conveyor until all 3 balls fired
		 4. Move off the Auto line
		 (Potentially turn 180 here, to shave some time off the next step)
		 6. Look for yellow ball with vision
		 7. Intake yellow ball
		 8-11. Find and intake 2 more yellow balls
		 (Potentially turn 180 here, to shave some time off the next step)
		 12. Target the retroreflective marks on the goal, using vision
		 13. Run conveyor to fire all 3 balls, again
		*/
	}
}