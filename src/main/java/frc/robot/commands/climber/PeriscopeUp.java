// /*----------------------------------------------------------------------------*/
// /* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
// /* Open Source Software - may be modified and shared by FRC teams. The code   */
// /* must be accompanied by the FIRST BSD license file in the root directory of */
// /* the project.                                                               */
// /*----------------------------------------------------------------------------*/

// package frc.robot.commands.climber;

// import edu.wpi.first.wpilibj2.command.CommandBase;
// import frc.robot.subsystems.Climber;

// public class PeriscopeUp extends CommandBase {
    
//   private final Climber m_climber;

//   public PeriscopeUp(Climber subsystem) {
//     super();
//     m_climber = subsystem;
//     addRequirements(m_climber);
//   }

//   //Called when the command is initially scheduled.
//   @Override
//   public void initialize() {
//     m_climber.deployPeriscope();
//   }

//   //Called every time the scheduler runs while the command is scheduled.
//   @Override
//   public void execute() {}

//   //Called once the command ends or is interrupted.
//   @Override
//   public void end(boolean interrupted) {
//     m_climber.stopPeriscope();
//   }

//   //Returns true when the command should end.
//   @Override
//   public boolean isFinished() {
//     return false;
//   }
// }