package frc.robot.commands.elevator;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
// import frc.robot.Debug;
import frc.robot.subsystems.Elevator;

public class RaiseElevator extends CommandBase {
    private final Elevator m_elevator;
    // private final Debug m_debugger = new Debug("Raise elevator command");


public RaiseElevator(Elevator subsystem) {
    m_elevator = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(m_elevator);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
   // m_debugger.log("Start puncher");
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
   // m_debugger.log("puncher, punch so the puncher is gonna move the ball up so it can shoot");
   m_elevator.raiseElevator();
   System.out.println("raising elevator");
  }

  @Override
	public void end(boolean interrupted) {
    
		m_elevator.stopElevator();
    System.out.println("stopping elevator");

		
	}

  // Called once the command ends or is interrupted.
  @Override
  public boolean isFinished() {
    //Nothing to do here, either
    //m_debugger.log("puncher extended successfully");
   return m_elevator.getElevatorUpperLimitSwitch();
 
    
  }

}
