/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.elevator;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Elevator;

public class ElevatorManual extends CommandBase {
	private final Elevator m_elevator;
	private final Joystick m_weaponsJoystick;

	//Basic joystick control for the drivetrain
	public ElevatorManual(Elevator subsystem, Joystick weaponsJoystick) {
		m_elevator = subsystem;
		m_weaponsJoystick = weaponsJoystick;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_elevator);
	}

	//Called when the command is initially scheduled.
	@Override
	public void initialize() {}

	//Called every time the scheduler runs while the command is scheduled.
	@Override
	public void execute() {
		m_elevator.elevatorMotorManually(m_weaponsJoystick);

		
	}

	//Called once the command ends or is interrupted.
	@Override
	public void end(boolean interrupted) {
		if (interrupted) {
			//replace these with the new Debugger
			System.out.println("elevator motor manual command interrupted");
		}
	
	}

	//Returns true when the command should end.
	@Override
	public boolean isFinished() {
		return false;
	}
}
//hello