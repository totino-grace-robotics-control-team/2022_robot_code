/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.autonomous;

import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.IntakeAndShoot;
import frc.robot.commands.drivetrain.AutoDrive;
import frc.robot.commands.intakeShooter.AimFixed;
import frc.robot.commands.intakeShooter.DecreaseMotorSpeed;
// import frc.robot.commands.intakeShooter.AimFixed;
import frc.robot.commands.intakeShooter.PerformShoot;
import frc.robot.commands.intakeShooter.ResetAimEncoder;
// import frc.robot.commands.intakeShooter.ResetAimEncoder;
// import frc.robot.commands.intakeShooter.StopShoot;
import frc.robot.subsystems.Aim;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/latest/docs/software/commandbased/convenience-features.html
public class AutonomousCommandGroup extends SequentialCommandGroup {
	/**
	 * Creates a new IntakeProcess.
	 */
	public AutonomousCommandGroup(Aim aimSS, IntakeAndShoot intakeAndShootSS, Drivetrain driveTrainSS) {
		// Add your commands in the super() call, e.g.
		 //super(new ResetAimEncoder(aimSS), new AimFixed(aimSS, 45), new PerformShoot(intakeAndShootSS));
        // super(new ResetAimEncoder(aimSS), new AimFixed(aimSS, -50), new PerformShoot(intakeAndShootSS, aimSS), new AutoDrive(driveTrainSS).withTimeout(1.5));
		//super(new IntakeBall(m_ics), new ConveyBallForIntake(m_ics));
		//super(new ResetAimEncoder(aimSS), new PerformShoot(intakeAndShootSS, aimSS), new AutoDrive(driveTrainSS).withTimeout(1.25));
		//super(new AutoDrive PerformShoot(intakeAndShootSS, aimSS), new WaitCommand(.25), new AutoDrive(driveTrainSS));
		//super(new AutoDrive(driveTrainSS).withTimeout(0.1), new ResetAimEncoder(aimSS), new AimFixed(aimSS, -15.0), new WaitCommand(0.5), new AimFixed(aimSS, -15.0), new PerformShoot(intakeAndShootSS, aimSS), new AutoDrive(driveTrainSS).withTimeout(1.5),  new AimFixed(aimSS, -75.0), new DecreaseMotorSpeed(intakeAndShootSS), new DecreaseMotorSpeed(intakeAndShootSS));
		//super(new AutoDrive(driveTrainSS).withTimeout(0.1), new ResetAimEncoder(aimSS), new AimFixed(aimSS, -30.0), new PerformShoot(intakeAndShootSS, aimSS));
		//super(new ResetAimEncoder(aimSS), new AimFixed(aimSS, -16.0), new PerformShoot(intakeAndShootSS, aimSS), new AutoDrive(driveTrainSS).withTimeout(1.5));
	//	super(new AutoDrive(driveTrainSS).withTimeout(0.1), new PerformShoot(intakeAndShootSS, aimSS), new AutoDrive(driveTrainSS).withTimeout(1.5));
		super(new AutoDrive(driveTrainSS).withTimeout(0.1), new ResetAimEncoder(aimSS), new AimFixed(aimSS, -25.0), new WaitCommand(0.5), new AimFixed(aimSS, -25.0), new PerformShoot(intakeAndShootSS, aimSS), new AutoDrive(driveTrainSS).withTimeout(1.5), new AimFixed(aimSS, -75.0));
}
}
