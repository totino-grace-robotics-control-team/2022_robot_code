/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.intakeShooter;

import frc.robot.Debug;
// import frc.robot.subsystems.IntakeAndShoot;
import frc.robot.Constants.subsystemDebuggers;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Aim;
/**
 * Picks up a ball off the ground
 */
public class AimFixed extends CommandBase {
	private final Debug m_debugger = new Debug("fixing aim motor");
	private final Aim m_aim;
    private double m_targetEncoderValue;
	private double m_angle;

	/**
	 * Creates a new IntakeBall Command.
	 *
	 * @param subsystem The subsystem used by this command.
	 */
	public AimFixed(Aim subsystem, double angle) {
		super();

		m_aim = subsystem;
		m_angle = angle;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_aim);

		if (subsystemDebuggers.kIcsDebug) {
			m_debugger.enable();
		}
	}

	// Called when the command is initially scheduled.
	@Override
	public void initialize() {
		
		m_debugger.log("starting command");
        
	}

	// Called every time the scheduler runs while the command is scheduled.
	@Override
	public void execute() {
	m_aim.setAngleAimMotor(m_angle);
	System.out.println("fixing aim");
	System.out.println(m_angle);
	}


	// Called once the command ends or is interrupted.
	@Override
	public void end(boolean interrupted) {
		System.out.println("ending aim fixed command");
        m_aim.stopAimMotor();

		
	}

	// Returns true when the command should end.
	@Override
	public boolean isFinished() {

		return m_aim.atAimTarget(m_angle);
	}
}
