/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.intakeShooter;

// import frc.robot.Debug;
// import frc.robot.subsystems.IntakeAndShoot;
// import frc.robot.Constants.subsystemDebuggers;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Aim;
/**
 * Picks up a ball off the ground
 */
public class PunchOnTest extends CommandBase {

	private final Aim m_aim;


	/**
	 * Creates a new IntakeBall Command.
	 *
	 * @param subsystem The subsystem used by this command.
	 */
	public PunchOnTest(Aim subsystem) {
		super();

		m_aim = subsystem;

		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_aim);

	}

	// Called when the command is initially scheduled.
	@Override
	public void initialize() {

        
	}

	// Called every time the scheduler runs while the command is scheduled.
	@Override
	public void execute() {
		System.out.println("punchON execute");
		// m_aim.punchOn();
	}

	// Called once the command ends or is interrupted.
	@Override
	public void end(boolean interrupted) {
		System.out.println("punchON  end");
	}

	// Returns true when the command should end.
	@Override
	public boolean isFinished() {

		return true;
	}
}
