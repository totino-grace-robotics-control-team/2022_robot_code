package frc.robot.commands.intakeShooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
// import frc.robot.Debug;
import frc.robot.subsystems.IntakeAndShoot;

public class DecreaseMotorSpeed extends CommandBase {
    private final IntakeAndShoot m_intakeAndShoot;
    // private final Debug m_debugger = new Debug("Puncher extension command");


public DecreaseMotorSpeed(IntakeAndShoot subsystem) {
    m_intakeAndShoot = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(m_intakeAndShoot);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    System.out.println("command is initialize");
   // m_debugger.log("Start puncher");
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    System.out.println("command is execute");
      m_intakeAndShoot.decreaseShooterSpeed();
   // m_debugger.log("puncher, punch so the puncher is gonna move the ball up so it can shoot");
    
  }

  @Override
	public void end(boolean interrupted) {
		
		
	}

  // Called once the command ends or is interrupted.
  @Override
  public boolean isFinished() {
    //Nothing to do here, either
    //m_debugger.log("puncher extended successfully");
    return true;
    
  }

}
