/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.intakeShooter;

import frc.robot.subsystems.Aim;
import frc.robot.subsystems.IntakeAndShoot;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/latest/docs/software/commandbased/convenience-features.html
public class PerformShoot extends SequentialCommandGroup {
	private static final double m_puncherExtendRuntime = 0.125;
	private static final double m_puncherRetractRuntime = 0.125;
	/**
	 * Creates a new IntakeProcess.
	 */
	public PerformShoot(IntakeAndShoot intakeAndShoot, Aim aim) {
		// Add your commands in the super() call, e.g.
		// super(new FooCommand(), new BarCommand());
		// super(new BeginShoot(intakeAndShoot), new WaitCommand(1.0), new ExtendPuncher(intakeAndShoot).withTimeout(m_puncherRuntime), new WaitCommand(0.75), new RetractPuncher(intakeAndShoot).withTimeout(m_puncherRuntime), new StopShoot(intakeAndShoot), new WaitCommand(3.0), new LowerAimer(aim));
		super(new BeginShoot(intakeAndShoot), new WaitCommand(1.5), new ExtendPuncher(intakeAndShoot).withTimeout(m_puncherExtendRuntime), new WaitCommand(0.75), new RetractPuncher(intakeAndShoot).withTimeout(m_puncherRetractRuntime), new ExtendPuncher(intakeAndShoot).withTimeout(m_puncherExtendRuntime), new WaitCommand(0.75), new RetractPuncher(intakeAndShoot).withTimeout(m_puncherRetractRuntime), new StopShoot(intakeAndShoot), new AimFixed(aim, -75.0));
		//super(new BeginShoot(intakeAndShoot), new WaitCommand(1.0), new WaitCommand(0.75), new StopShoot(intakeAndShoot), new WaitCommand(3.0));

	}
}
