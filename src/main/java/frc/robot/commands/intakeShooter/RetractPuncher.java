
package frc.robot.commands.intakeShooter;

import edu.wpi.first.wpilibj2.command.CommandBase;
// import frc.robot.Debug;
import frc.robot.subsystems.IntakeAndShoot;

public class RetractPuncher extends CommandBase {
    private final IntakeAndShoot m_intakeAndShoot;
    // private final Debug m_debugger = new Debug("Puncher retraction command");
    // private boolean isFinished = false;

public RetractPuncher(IntakeAndShoot subsystem) {
    m_intakeAndShoot = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(m_intakeAndShoot);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    //m_debugger.log("Start retracting puncher");
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    //m_debugger.log("puncher, retract so you are in starting position");
    m_intakeAndShoot.setPuncherMotor(0.4);
    // isFinished = false;
  }

  @Override
	public void end(boolean interrupted) {
	m_intakeAndShoot.stopPuncherMotor();
		
	}

  // Called once the command ends or is interrupted.
  @Override
  public boolean isFinished() {
    //Nothing to do here, either
    //m_debugger.log("puncher retracted successfully");
    return false;
    
    
  }

}