/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.intakeShooter;

import frc.robot.Debug;
import frc.robot.subsystems.IntakeAndShoot;
import frc.robot.Constants.subsystemDebuggers;
import edu.wpi.first.wpilibj2.command.CommandBase;

/**
 * Picks up a ball off the ground
 */
public class IntakeBall extends CommandBase {
	private final Debug m_debugger = new Debug("ICS command intake ball");
	private final IntakeAndShoot m_intakeAndShoot;

	/**
	 * Creates a new IntakeBall Command.
	 *
	 * @param subsystem The subsystem used by this command.
	 */
	public IntakeBall(IntakeAndShoot subsystem) {
		super();
		
		m_intakeAndShoot = subsystem;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_intakeAndShoot);

		if (subsystemDebuggers.kIcsDebug) {
			m_debugger.enable();
		}
		m_debugger.log("created IntakeBall command");
	}

	// Called when the command is initially scheduled.
	@Override
	public void initialize() {
		
		m_debugger.log("starting command");
	}
	

	// Called every time the scheduler runs while the command is scheduled.
	@Override
	public void execute() {
		if(!m_intakeAndShoot.isBallInShooter()){
			m_intakeAndShoot.startIntake();
		}
	}

	// Called once the command ends or is interrupted.
	@Override
	public void end(boolean interrupted) {
		System.out.println("intake ball has been interupted "+ interrupted);
		m_intakeAndShoot.stopIntake();
		// m_ics.m_numberOfBallsContained += 1;
		
        m_debugger.log("command ended");
	}

	// Returns true when the command should end.
	@Override
	public boolean isFinished() {
		if (m_intakeAndShoot.isBallInShooter()) {
			m_debugger.log("done intaking because ball is in");
			return true;
		}

		return false;
	}
}
