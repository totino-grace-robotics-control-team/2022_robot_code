/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.intakeShooter;

import frc.robot.Debug;
// import frc.robot.subsystems.IntakeAndShoot;
import frc.robot.Constants.subsystemDebuggers;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Aim;
/**
 * Picks up a ball off the ground
 */
public class ResetAimEncoder extends CommandBase {
	private final Debug m_debugger = new Debug("fixing aim motor");
	private final Aim m_aim;
    // private double m_targetEncoderValue;
	// private double m_angle;

	/**
	 * Creates a new IntakeBall Command.
	 *
	 * @param subsystem The subsystem used by this command.
	 */
	public ResetAimEncoder(Aim subsystem) {
		super();

		m_aim = subsystem;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_aim);

		if (subsystemDebuggers.kIcsDebug) {
			m_debugger.enable();
		}
	}

	// Called when the command is initially scheduled.
	@Override
	public void initialize() {
		m_aim.deployMotor(-0.35);
		m_debugger.log("starting command");
        
	}

	// Called every time the scheduler runs while the command is scheduled.
	@Override
	public void execute() {
		System.out.println("resetting aim!");
	}

	// Called once the command ends or is interrupted.
	@Override
	public void end(boolean interrupted) {
		System.out.println("at end of reset aim");
        m_aim.stopAimMotor();
        m_aim.setAimEncoderZero();
		System.out.println("ending resetting aim encoder");
        //TODO stop aim motor


		
	}

	// Returns true when the command should end.
	@Override
	public boolean isFinished() {
		System.out.println(m_aim.getUpperLimitSwitch());
		return m_aim.getUpperLimitSwitch();
		
		
	}
}
