/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.intakeShooter;

// import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Debug;
import frc.robot.subsystems.IntakeAndShoot;

public class StopShoot extends CommandBase {
  private final IntakeAndShoot m_intakeAndShoot;
  private final Debug m_debugger = new Debug("IntakeAndShoot stop shooter command");

  /**
   * Creates a new ShootContinuous.
   */
  public StopShoot(IntakeAndShoot subsystem) {
    m_intakeAndShoot = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(m_intakeAndShoot);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {} 

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    //Nothing to do here
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    //Nothing to do here, either
   // Timer.delay(5);
    m_intakeAndShoot.stopShooter();
    m_debugger.log("StopShoot complete");
  }

  //isFinished function removed, replaced by isFinished boolean
  @Override
	public boolean isFinished() {
	
			return true;
	}

}