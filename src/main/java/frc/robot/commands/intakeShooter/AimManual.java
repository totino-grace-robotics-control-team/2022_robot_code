/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.intakeShooter;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Aim;

public class AimManual extends CommandBase {
	private final Aim m_aim;
	private final Joystick m_weaponsJoystick;

	//Basic joystick control for the drivetrain
	public AimManual(Aim subsystem, Joystick weaponsJoystick) {
		m_aim = subsystem;
		m_weaponsJoystick = weaponsJoystick;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_aim);
	}

	//Called when the command is initially scheduled.
	@Override
	public void initialize() {}

	//Called every time the scheduler runs while the command is scheduled.
	@Override
	public void execute() {
		m_aim.aimMotorManually(m_weaponsJoystick);
		System.out.println(m_aim.getUpperLimitSwitch());
		//System.out.println(m_aim.getLowerLimitSwitch());
	}

	//Called once the command ends or is interrupted.
	@Override
	public void end(boolean interrupted) {
		if (interrupted) {
			//replace these with the new Debugger
			System.out.println("some other command has taken over the Aim Subsystem!");
		} else {
			System.out.println("this should never happen, but the joystick drive command has ended");
		}
	}

	//Returns true when the command should end.
	@Override
	public boolean isFinished() {
		//The command should never end.
		return false;
	}
}
