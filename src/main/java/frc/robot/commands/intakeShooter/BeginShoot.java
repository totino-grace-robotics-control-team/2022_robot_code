/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.intakeShooter;

import frc.robot.Debug;
import frc.robot.subsystems.IntakeAndShoot;
import frc.robot.Constants.subsystemDebuggers;
// import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
// import edu.wpi.first.wpilibj2.command.WaitCommand;

/**
 * Picks up a ball off the ground
 */
public class BeginShoot extends CommandBase {
	private final Debug m_debugger = new Debug("ICS command intake ball");
	private final IntakeAndShoot m_intakeAndShoot;

	/**
	 * Creates a new IntakeBall Command.
	 *
	 * @param subsystem The subsystem used by this command.
	 */
	public BeginShoot(IntakeAndShoot subsystem) {

		m_intakeAndShoot = subsystem;
		// Use addRequirements() here to declare subsystem dependencies.
		addRequirements(m_intakeAndShoot);

		if (subsystemDebuggers.kIcsDebug) {
			m_debugger.enable();
		}
	}

	// Called when the command is initially scheduled.
	@Override
	public void initialize() {
		m_intakeAndShoot.startShooter();
		m_debugger.log("starting command");
	}

	// Called every time the scheduler runs while the command is scheduled.
	@Override
	public void execute() {
		// Timer.delay(3);

	}

	// Called once the command ends or is interrupted.
	@Override
	public void end(boolean interrupted) {
		// m_debugger.log("stop shoot");
		// m_intakeAndShoot.stopShooter();
		
	}

	// Returns true when the command should end.
	 @Override
	public boolean isFinished() {

	return true;
	}
}
