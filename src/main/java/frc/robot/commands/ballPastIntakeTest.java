package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeAndShoot;

public class ballPastIntakeTest extends CommandBase {

    // track the subsystem this command is for
    private final IntakeAndShoot m_intakeAndShoot;

    /**
     * constructor
     * 
     * @param subsystem
     */
    public ballPastIntakeTest(IntakeAndShoot subsystem) {
        super();
        m_intakeAndShoot = subsystem;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(m_intakeAndShoot);
    }

    @Override
    public void initialize() {
        System.out.println("initializing ball past intake test");
    }

    // Called every time the scheduler runs while the command is scheduled.
    @Override
    public void execute() {
        System.out.println("extend ball past intake test");
        //m_intakeAndShoot.startPuncherMotor();
        // m_intakeAndShoot.retractEnterShootServo();
        // System.out.println("initializing ball past intake test");
        // System.out.println("cmd execute: ball past intake test");
        // m_intakeAndConvey.getballpassintakestate();
        // m_intakeAndConvey.startConveyor();
    }

    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {
        // m_intakeAndConvey.stopConveyor();
    }

    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
    
        //System.out.println("cmd isFinished: ball past intake test");
        // since isFinished is called repeatedly until it returns true,
        // can also leverage this to test a subsystem method, and use it's status
        // m_intakeAndConvey.getballpassintakestate();
        return true;
    }

}